package ru.webserver.ClientSession;


import ru.webserver.HttpResponceGET.HttpGet;
import ru.webserver.HttpResponcePOST.HttpPost;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class ClientSession implements Runnable {

    @Override
    public void run() {
        try(BufferedReader input = new BufferedReader(
                new InputStreamReader(
                        socket.getInputStream(), StandardCharsets.UTF_8)
                )
        ) {
            while(!input.ready());

            String firstLine = input.readLine();
            String method = getMethodFromHttp(firstLine);
            System.out.println(firstLine);

            while (input.ready()){
                System.out.println(input.readLine());
            }
            
            if(method.equals("GET")){
                
            } else if (method.equals("POST")) {
                
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public ClientSession(Socket clientSocket){
        this.socket = clientSocket;
    }

    private static String getMethodFromHttp(String http){
        String result = "GET";
        http.split(" ");
        if (http.equals("POST")){
            result = "POST";
        }
        return result;
    }

    private Socket socket;
}