package ru.webserver;


import ru.webserver.ClientSession.ClientSession;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class HttpServer {
    public static void main(String... args) {

        int port = DEFAULT_PORT;
        if (args.length > 0) {
            port = Integer.parseInt(args[0]);
        }

        try (ServerSocket serverSocket = new ServerSocket(8080)) {
            System.out.println("Server started on port: "
                    + serverSocket.getLocalPort() + "\n");


            while (true) {
                try {
                    Socket clientSocket = serverSocket.accept();
                    ClientSession session = new ClientSession(clientSocket);
                    new Thread(session).start();
                } catch (IOException e) {
                    System.out.println("Failed to establish connection.");
                    System.out.println(e.getMessage());
                    System.exit(-1);
                }
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    private static final int DEFAULT_PORT = 8080;
}